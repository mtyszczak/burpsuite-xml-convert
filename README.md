# burpsuite-xml-convert

Based on the [burp-suite-http-proxy-history-converter](https://github.com/mrts/burp-suite-http-proxy-history-converter)

Python script that converts Burp Suite HTTP proxy history files to HTML or CSV.

The history file can be exported from Burp Suite by opening *Proxy > HTTP
History*, selecting relevant records, right-clicking and choosing *Save items*.

Besides this converter, a Java-based [standalone Burp Suite HTTP history
viewer](https://github.com/mrts/burp-suite-http-proxy-history-viewer) is also
available.

## Usage

Download the script and install requirements:

    $ git clone https://github.com/mtyszczak/burpsuite-xml-convert.git
    $ cd burpsuite-xml-convert
    $ pip3 install --requirement=requirements.txt

Usage overview:

    $ python3 burpsuite-xml-convert.py -h
    usage: burpsuite-xml-convert.py [-h] [--format {html,csv}] [--csv-delimiter {,,;}] [--base64encode] filename

    Python script that converts Burp Suite HTTP proxy history files to CSV or HTML files

    positional arguments:
        filename              Burp Suite HTTP proxy history file

    options:
        -h, --help            show this help message and exit
        --format {html,csv}   output format, default: html
        --csv-delimiter {,,;}
                              CSV delimiter, default: ,
        --base64encode        Enable if the source has been base64 encoded, default: false

Convert Burp Suite HTTP proxy history file to HTML, output will be next to input
file with `.html` extension:

    python3 burpsuite-xml-convert.py example/burp-http-history.xml

Convert Burp Suite HTTP proxy history file to CSV using `;` as delimiter, output
will be next to input file with `.csv` extension (also mark that input burpsuite history file has been base64-encoded):

    python burpsuite-xml-convert.py example/burp-http-history.xml \
        --format csv --csv-delimiter ';' --base64encode

**Note that CSV file fields are truncated to 32760 characters as that is the
total number of characters that an Excel cell can contain.**
