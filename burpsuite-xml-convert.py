"""
Python script that converts Burp Suite HTTP proxy history files to CSV or HTML files
"""
from __future__ import unicode_literals
from __future__ import print_function

import sys
import io
import argparse
import html
import base64

import xmltodict
from backports import csv

_g_csv_delimiter = ','
_g_base64encode = False

def main():
    args = parse_arguments()
    set_csv_delimiter(args.csv_delimiter)
    format_handler = FORMATS[args.format](args.filename)
    http_history = parse_http_history(args.filename)
    set_base64encode(args.base64encode)
    convert_to_output_file(http_history, format_handler)


def parse_arguments():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('filename', help='Burp Suite HTTP proxy history file')
    parser.add_argument('--format', default='html', choices=FORMATS.keys(),
                        help='output format, default: html')
    parser.add_argument('--csv-delimiter', choices=(',', ';'),
                        help='CSV delimiter, default: ,')
    parser.add_argument('--base64encode', action='store_true',
                        help='Enable if the source has been base64 encoded, default: false')
    return parser.parse_args()


def convert_to_output_file(http_history, format_handler):
    with io.open(format_handler.filename, 'w', encoding='utf-8', newline='') as output_file:
        format_handler.set_output_file(output_file)
        format_handler.header_prefix()
        format_handler.header_column('Time')
        format_handler.header_column('URL')
        format_handler.header_column('Hostname')
        format_handler.header_column('IP address')
        format_handler.header_column('Port')
        format_handler.header_column('Protocol')
        format_handler.header_column('Method')
        format_handler.header_column('Path')
        format_handler.header_column('Extension')
        format_handler.header_column('Request')
        format_handler.header_column('Status')
        format_handler.header_column('Response length')
        format_handler.header_column('MIME type')
        format_handler.header_column('Response')
        format_handler.header_column('Comment')
        format_handler.header_suffix()
        for line in http_history['items']['item']:
            format_handler.row_prefix()
            format_handler.row_column(line['time'])
            format_handler.row_column(line['url'])
            format_handler.row_column(line['host']['#text'])
            format_handler.row_column(line['host']['@ip'])
            format_handler.row_column(line['port'])
            format_handler.row_column(line['protocol'])
            format_handler.row_column(line['method'])
            format_handler.row_column(line['path'])
            format_handler.row_column(line['extension'])
            if '#text' in line['request']:
                format_handler.row_column(line['request']['#text'], encoded=True)
            else:
                format_handler.row_column("None")
            format_handler.row_column(line['status'])
            format_handler.row_column(line['responselength'])
            format_handler.row_column(line['mimetype'])
            if '#text' in line['response']:
                format_handler.row_column(line['response']['#text'], encoded=True)
            else:
                format_handler.row_column("None")
            format_handler.row_column(line['comment'])
            format_handler.row_suffix()
        format_handler.footer()


def parse_http_history(filename):
    with open(filename, 'rb') as f:
        return xmltodict.parse(f)


def base64decode(line):
    if not _g_base64encode:
        return line
    decoded = base64.b64decode(line)
    replace = 'backslashreplace' if sys.version_info[0] >= 3 else 'replace'
    return decoded.decode('UTF-8', errors=replace)

def set_base64encode(base64encode):
    if base64encode:
        global _g_base64encode
        _g_base64encode = unicode(base64encode)

def set_csv_delimiter(csv_delimiter):
    if csv_delimiter:
        global _g_csv_delimiter
        _g_csv_delimiter = unicode(csv_delimiter)


class FormatHandlerBase(object):
    def __init__(self, filename):
        self.filename = filename + self.FILENAME_SUFFIX


class HtmlFormatHandler(FormatHandlerBase):
    FILENAME_SUFFIX = '.html'
    HEADER = '''<!DOCTYPE html><html><head><meta charset="utf-8">
      <link rel="stylesheet" type="text/css"
      href="https://cdn.datatables.net/v/dt/jq-3.6.0/dt-1.12.1/b-2.2.3/b-print-2.2.3/r-2.3.0/datatables.min.css"/>
      <script type="text/javascript"
      src="https://cdn.datatables.net/v/dt/jq-3.6.0/dt-1.12.1/b-2.2.3/b-print-2.2.3/r-2.3.0/datatables.min.js"></script>
      <style>pre{overflow-wrap:anywhere;max-width:calc(95vw);white-space:pre-line;}#cd{filter:invert(100%) hue-rotate(18deg) brightness(4.5);}#t_paginate *{color: #fff !important}.dataTables_wrapper .dataTables_filter input{color:#fff}.paginate_button.disabled,.dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover,.dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active{color:#fff !important}.dataTables_wrapper .dataTables_length,.dataTables_wrapper .dataTables_filter,.dataTables_wrapper .dataTables_info,.dataTables_wrapper .dataTables_processing,.dataTables_wrapper .dataTables_paginate{color:#fff !important;}button.dt-button,div.dt-button,a.dt-button,input.dt-button{color:#fff !important}body{background:#000;color:#fff !important;}:root{--theadColor:#000;--theadTextColor:#fff;--darkColor:#000;--lightColor:#fff;--darkRowColor:#1b1d1b}body{font-family:"Open Sans",sans-serif}table.dataTable{border:1px solid #000;background-color:#000}td,th,tr{border-color:#000!important}thead{background-color:var(--theadColor)}thead>tr,thead>tr>th{background-color:transparent;color:var(--theadTextColor)!important;font-weight:400;text-align:start}table.dataTable thead td,table.dataTable thead th{border-bottom:0 solid #111!important}.dataTables_wrapper>div{margin:5px}table.dataTable.display tbody tr.even,table.dataTable.display tbody tr.even>.sorting_1,table.dataTable.display tbody tr.odd,table.dataTable.display tbody tr.odd>.sorting_1,table.dataTable.order-column.stripe tbody tr.even>.sorting_1,table.dataTable.order-column.stripe tbody tr.odd>.sorting_1{background-color:var(--darkRowColor);color:var(--lightColor)}table.dataTable thead th{position:relative;background-image:none!important}table.dataTable thead th.sorting:after,table.dataTable thead th.sorting_asc:after,table.dataTable thead th.sorting_desc:after{position:absolute;top:12px;right:8px;display:block;font-family:\"Font Awesome 5 Free\"}table.dataTable thead th.sorting:after{content:\"\f0dc\";color:#ddd;font-size:.8em;padding-top:.12em}table.dataTable thead th.sorting_asc:after{content:\"\f0de\"}table.dataTable thead th.sorting_desc:after{content:\"\f0dd\"}table.dataTable.display tbody tr:hover>.sorting_1,table.dataTable.order-column.hover tbody tr:hover>.sorting_1,tbody tr:hover{background-color:var(--darkColor)!important;color:#fff}.dataTables_wrapper .dataTables_paginate .paginate_button.current,.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover{background:0 0!important;border-radius:50px;background-color:var(--theadColor)!important;color:var(--lightColor)!important}.dataTables_wrapper .dataTables_paginate .paginate_button{background:0 0!important;color:var(--darkColor)!important}.paginate_button.current:hover{background:0 0!important;border-radius:50px;background-color:var(--theadColor)!important;color:#fff!important}.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover,.dataTables_wrapper .dataTables_paginate .paginate_button:hover{border:1px solid #979797;background:#000!important;border-radius:50px!important;color:#fff!important}</style>
      </head><body><table id="t" class="display">
      <caption>Burpsuite dump</caption><thead><tr>
'''
    FOOTER = '''</tbody></table>
    <script>$("#t").DataTable({dom:'Bfrtip',responsive:true,buttons:['print']})</script>
    </body></html>
'''

    def set_output_file(self, output_file):
        self.output_file = output_file

    def header_prefix(self):
        print(self.HEADER,
              file=self.output_file)

    def header_suffix(self):
        print('</tr></thead><tbody>',
              file=self.output_file)

    def header_column(self, column_name):
        print('<th>%s</th>' % column_name,
              file=self.output_file)

    def row_prefix(self):
        print('<tr>', file=self.output_file)

    def row_suffix(self):
        print('</tr>', file=self.output_file)

    def row_column(self, content, encoded=False):
        template = '<td>%s</td>' if not encoded else '<td><pre>%s</pre></td>'
        if encoded:
            content = html.escape(
                base64decode(
                    content
                )
            )
        print(template % content,
              file=self.output_file)

    def footer(self):
        print(self.FOOTER,
              file=self.output_file)


# note that total number of characters that an Excel cell can contain is 32,760
class CsvFormatHandler(FormatHandlerBase):
    FILENAME_SUFFIX = '.csv'

    def set_output_file(self, output_file):
        self.writer = csv.writer(output_file, dialect='excel',
                                 delimiter=_g_csv_delimiter)
        self.header = []

    def header_prefix(self):
        pass

    def header_suffix(self):
        self.writer.writerow(self.header)

    def header_column(self, column_name):
        self.header.append(column_name)

    def row_prefix(self):
        self.row = []

    def row_suffix(self):
        self.writer.writerow(self.row)

    def row_column(self, content, encoded=False):
        if content and encoded:
            content = base64decode(content)
        # total number of characters that an Excel cell can contain is 32,760
        if content and len(content) > 32760:
            content = content[:32744] + '..[TRUNCATED!]'
        self.row.append(content)

    def footer(self):
        pass


FORMATS = {
    'html': HtmlFormatHandler,
    'csv': CsvFormatHandler,
}

if __name__ == '__main__':
    # In python 3, unicode is renamed to str
    if sys.version_info[0] >= 3:
      unicode = str
    main()
